#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define columns 80
#define lines 25
#define step_to_x 0.157080
#define step_to_y 0.080000

void Show_Graph(char *matrix);
void Calculating_Points(char *matrix);

int main() {
    char *array = (char *)malloc(columns * lines * sizeof(char));
    Calculating_Points(array);
//    for (int i = 0; i < columns*lines; ++i) {
//        for (int j = 0; j < columns; ++j) {
//        if ( i % 2 != 0 && j % 2 != 0 && i*j % 3 != 1)
//            *(array+i*columns + j) = 1;
//        else
//            *(array+i*columns + j) = 0;
//        }
//    }
    Show_Graph(array);
    free(array);
    return 0;
}

void Calculating_Points(char *matrix) {
    for (int i = columns-1; i > 0; --i) {
        double x = i * step_to_x;
        double y =0.2/log(x)+sin(x);
        if ( y <= 1 && y >= -1 )
        {
            int j = y / step_to_y;
            if ( y <= 1 || y >= -1 )
                matrix[(-j+12)*columns + i] = 1;
        }
    }
}

void Show_Graph(char *matrix) {
    for (int i = 0; i < lines; ++i) {
        for (int j = 0; j < columns; ++j) {
            if ( matrix[i*columns + j] == 0 )
            printf("%c", '.');
            if ( matrix[i*columns + j] == 1 )
            printf("%c", '*');
        }
        printf("\n");
    }
}
